import Test.Framework
import Test.Framework.Providers.HUnit
import Test.HUnit

import TrackTest

main :: IO ()
main = defaultMain 
    [ testTrueRadius
    , testPieceLength
    , testSections]
   

