module TrackTest where

import Data.Aeson(decode, fromJSON, Result(..), Value(..))
import Data.List(nub)
import qualified Data.ByteString.Lazy.Char8 as L
import Test.Framework
import Test.Framework.Providers.HUnit
import Test.HUnit

import Model.Message
import Model.Race
import Model.Track
import ServerMessage

tBendLeft = Piece Nothing Nothing (Just 100) (Just (-45)) Nothing
tBendRight = Piece Nothing Nothing (Just 100) (Just 45) Nothing

tLeftLane = Lane (-10) 0
tRightLane = Lane 10 0

-- `trueRadius`

testTrueRadiusWhenTurningLeft = 
    let (Just b) = do
        lRad <- trueRadius tBendLeft tLeftLane
        rRad <- trueRadius tBendLeft tRightLane
        return $ lRad < rRad
    in b @?= True

testTrueRadiusWhenTurningRight =
    let (Just b) = do
        lRad <- trueRadius tBendRight tLeftLane
        rRad <- trueRadius tBendRight tRightLane
        return $ lRad > rRad
    in b @?= True

testTrueRadius = testGroup "Test track data and functions"
    [ testCase "trueRadius when turning left" 
            testTrueRadiusWhenTurningLeft
    , testCase "trueRadius when turning right" 
            testTrueRadiusWhenTurningRight
    ]

-- `pieceLength`

testPieceLengthWhenTurningLeft = 
    let t = pieceLength tBendLeft 
    in t tLeftLane < t tRightLane @?= True

testPieceLengthWhenTurningRight = 
    let t = pieceLength tBendRight 
    in t tLeftLane > t tRightLane @?= True

testPieceLength = testGroup "Test `pieceLength`"
    [ testCase ("Piece length in the left lane is not shorter "
                ++ "than in the right lane.")
        testPieceLengthWhenTurningLeft
    , testCase ("Piece length in the right lane is not shorter "
                ++ "than in the left lane.")
        testPieceLengthWhenTurningRight
    ]

-- `sections`

testSectionsDoesNotDropAnyPiece track sections = 
        len @?= (sum $ map pcs sections)
    where
        len = length $ pieces track
        pcs (Switch _ _) = 1
        pcs (Section xs _) = length xs

testSectionsOrderIsCorrect track sections = 
        sequence_ . zipWith (@=?) [0..] $ concatMap ids sections
    where
        ids (Switch (idx,_) _) = [idx]
        ids (Section xs _) = map fst xs

testSections = testGroup "`sections` function."
    [ testCase "sections does not drop any pieces"
        (testSectionsDoesNotDropAnyPiece track sections')
    , testCase "Sections order is correct after sections -function"
        (testSectionsOrderIsCorrect track sections')]
    where
        (Just track) = testTrack1
        sections' = sections track

-- `sameBend`

testSameBend = testGroup "`sameBend` function"
    [ testCase "Right bend failure" 
        (sameBend tBendRight tBendRight @?= (Just True))
    , testCase "Left bend failure"
        (sameBend tBendLeft tBendLeft  @?= (Just True))
    , testCase "Bend failure"
        (sameBend tBendLeft tBendRight @?= (Just True))
    , testCase "Bend failure"
        (sameBend tBendRight tBendLeft @?= (Just True))
    ]

-- `slice`
testSlice = testGroup "`slice` function"
    [ testCase "When starting from 0 length is same"
        (propSliceLengthIsSameAsListLength [1..6] 3 @?= True)
    , testCase "When starting from 0 returns same as `take len`"
        (slice 0 3 [1..6] @?= take 3 [1..6]) 
    , testCase "slice (-1) 2 [1..4] == [1]"
        (slice (-1) 2 [1..4] @?= [1])
    ]

propSliceLengthIsSameAsListLength xs len 
    = length (slice 0 len xs) == length xs

-- Testing helpers

testTrack1 :: Maybe Track
testTrack1 = case testMsg of
    Success (GameInitMsg gameInit) 
        -> Just . track . race $ gameInit
    _   -> Nothing

testMsg :: (Result ServerMessage)
testMsg = case testJson of
    Just v  -> fromJSON v
    _       -> Error "nothing"

testJson :: Maybe Value
testJson = decode . L.pack $ "{\"msgType\":\"gameInit\","
    ++ "\"data\":{\"race\":{\"track\":{\"id\":\"keimola\",\"name\":\"Keimola\",\"pieces\":[{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":200,\"angle\":22.5,\"switch\":true},{\"length\":100.0},{\"length\":100.0},{\"radius\":200,\"angle\":-22.5},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":200,\"angle\":22.5},{\"radius\":200,\"angle\":-22.5},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"length\":62.0},{\"radius\":100,\"angle\":-45.0,\"switch\":true},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"length\":100.0,\"switch\":true},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":90.0}],\"lanes\":[{\"distanceFromCenter\":-10,\"index\":0},{\"distanceFromCenter\":10,\"index\":1}],\"startingPoint\":{\"position\":{\"x\":-300.0,\"y\":-44.0},\"angle\":90.0}},\"cars\":[{\"id\":{\"name\":\"Kaarne\",\"color\":\"red\"},\"dimensions\":{\"length\":40.0,\"width\":20.0,\"guideFlagPosition\":10.0}}],\"raceSession\":{\"laps\":3,\"maxLapTimeMs\":60000,\"quickRace\":true}}},\"gameId\":\"3627456e-0947-4bb9-83ae-1f040efa0697\"}"
