{-# LANGUAGE OverloadedStrings #-}
module ClientMessage where

import Data.Aeson(Value(..), ToJSON(..), (.=), object)

import Model.Message

data ClientMessage = ClientJoinMsg !Join
        | ClientJoinRaceMsg !JoinRace
        | PingMsg !Ping
        | ThrottleMsg !Throttle
        | SwitchMsg !Switch
        | Close
        deriving Show

instance ToJSON ClientMessage where
    toJSON (ClientJoinMsg p) = message "join" p
    toJSON (ClientJoinRaceMsg p) = message "joinRace" p
    toJSON (PingMsg p) = message "ping" p
    toJSON (ThrottleMsg p) = message "throttle" p
    toJSON (SwitchMsg p) = message "switchLane" p
    toJSON (Close) = message "quit" Null

message :: ToJSON a => String -> a -> Value
message msgType o = object 
            [ "msgType" .= msgType
            , "data"    .= o]
