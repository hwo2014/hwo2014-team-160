{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}

module Main where

import System.Environment (getArgs)
import System.Exit (exitFailure)

import Network(connectTo, PortID(..))
import System.IO(hPutStrLn, hGetLine, hSetBuffering, BufferMode(..), Handle, hClose)
import Data.List
import Control.Monad
import Control.Applicative
import qualified Data.ByteString.Lazy.Char8 as L
import Data.Aeson(encode, decode, FromJSON(..), fromJSON, parseJSON, eitherDecode, Value(..), (.:), Result(..), toJSON)

import Logic
import Model.Car
import Model.Race
import Model.Track
import Model.Message
import ClientMessage
import ServerMessage

throttleMessage = ThrottleMsg . Throttle
switchLaneMessage = SwitchMsg
pingMessage = PingMsg Ping

connectToServer server port = connectTo server (PortNumber (fromIntegral (read port :: Integer)))

row mem = [show . lastTick, throttle', speed', angle]
    where
        throttle' = show . memThrottle 
        speed' = maybe "" show . speed
        angle =  maybe "" show . memPosition

main = do
    args <- getArgs
    case args of
        [server, port, botname, botkey] -> 
            run server port botname botkey
        _ -> do
            putStrLn "Usage: hwo2014bot <host> <port> <botname> <botkey>"
            exitFailure

run server port botname botkey = do
    putStrLn "Connecting to server."
    h <- connectToServer server port
    hSetBuffering h LineBuffering
    hPutStrLn h . L.unpack . encode 
                . toJSON . ClientJoinMsg 
                $ Join botname botkey
--    hPutStrLn h . L.unpack . encode 
--                . toJSON . ClientJoinRaceMsg 
--                $ JoinRace (BotId botname botkey) Nothing Nothing 1
    handleMessages (initial botname) h
    hClose h

handleMessages :: Memory -> Handle -> IO Memory
handleMessages mem h = do
    msg <- hGetLine h
    case decode (L.pack msg) of
        Just json ->
            case fromJSON json of
                Success serverMsg -> handleServerMessage mem h serverMsg
                Error s -> fail $ "Error decoding message: " ++ s
        Nothing ->
            fail $ "Error parsing JSON: " ++ show msg

handleServerMessage :: Memory -> Handle -> ServerMessage -> IO Memory
handleServerMessage mem h serverMessage = do
    (responses, mem') <- respond mem serverMessage
    case responses of
        ([Close]) -> putStrLn "End" >> return mem
        (rs) -> do
                forM_ rs (hPutStrLn h . L.unpack . encode . toJSON)
                mem' `seq` handleMessages mem' h
                -- handleMessages mem' h

respond :: Memory -> ServerMessage -> IO ([ClientMessage], Memory)
respond memory msg = logicHandler msg memory

-- Testing stuff

testTrack :: Maybe Track
testTrack = case testMsg of
    Success (GameInitMsg gameInit) 
        -> Just . track . race $ gameInit
    _   -> Nothing

testMsg :: (Result ServerMessage)
testMsg = case testJson of
    Just v  -> fromJSON v
    _       -> Error "nothing"

testJson :: Maybe Value
testJson = decode . L.pack $ "{\"msgType\":\"gameInit\","
    ++ "\"data\":{\"race\":{\"track\":{\"id\":\"keimola\",\"name\":\"Keimola\",\"pieces\":[{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":200,\"angle\":22.5,\"switch\":true},{\"length\":100.0},{\"length\":100.0},{\"radius\":200,\"angle\":-22.5},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":200,\"angle\":22.5},{\"radius\":200,\"angle\":-22.5},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"length\":62.0},{\"radius\":100,\"angle\":-45.0,\"switch\":true},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"length\":100.0,\"switch\":true},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":90.0}],\"lanes\":[{\"distanceFromCenter\":-10,\"index\":0},{\"distanceFromCenter\":10,\"index\":1}],\"startingPoint\":{\"position\":{\"x\":-300.0,\"y\":-44.0},\"angle\":90.0}},\"cars\":[{\"id\":{\"name\":\"Kaarne\",\"color\":\"red\"},\"dimensions\":{\"length\":40.0,\"width\":20.0,\"guideFlagPosition\":10.0}}],\"raceSession\":{\"laps\":3,\"maxLapTimeMs\":60000,\"quickRace\":true}}},\"gameId\":\"3627456e-0947-4bb9-83ae-1f040efa0697\"}"

