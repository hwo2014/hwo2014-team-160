{-# LANGUAGE OverloadedStrings #-}

module Model.Car where

import Data.Aeson ((.:), (.:?), decode, encode, (.=), object, FromJSON(..), ToJSON(..), Value(..))
import Control.Applicative ((<$>), (<*>))
import Control.Monad        (liftM)

import Data.List

-- | Dimension
data Dimension = Dimension 
    { dimensionLength   :: Float
    , width             :: Float
    , guideFlagPosition :: Float
    } deriving (Show)

instance FromJSON Dimension where
    parseJSON (Object v) = 
        Dimension <$>
        (v .: "length") <*>
        (v .: "width") <*>
        (v .: "guideFlagPosition")

-- CarId

data CarId = CarId 
    { color     :: String
    , carIdName :: String
    } deriving (Show)

instance FromJSON CarId where
    parseJSON (Object v) = 
        CarId <$>
        (v .: "color") <*>
        (v .: "name")

-- | Car
data Car = Car 
    { carId         :: CarId
    , dimensions :: Dimension
    } deriving (Show)

instance FromJSON Car where
    parseJSON (Object v) =
        Car <$>
        (v .: "id") <*>
        (v .: "dimensions")


-- | CarLane
data CarLane = CarLane 
    { startLaneIndex :: Int
    , endLaneIndex   :: Int
    } deriving (Show)

instance FromJSON CarLane where
    parseJSON (Object v) =
        CarLane <$>
        (v .: "startLaneIndex") <*>
        (v .: "endLaneIndex")


-- | PiecePosition
data PiecePosition = PiecePosition 
    { pieceIndex      :: Int
    , inPieceDistance :: Float
    , lane            :: CarLane
    , lap             :: Int
    } deriving (Show)

instance FromJSON PiecePosition where
    parseJSON (Object v) =
        PiecePosition <$>
        (v .: "pieceIndex")      <*>
        (v .: "inPieceDistance") <*>
        (v .: "lane")            <*>
        (v .: "lap")

-- CarPosition
data CarPosition = CarPosition 
    { posCarId      :: CarId
    , carAngle      :: Float
    , piecePosition :: PiecePosition
    } deriving (Show)

instance FromJSON CarPosition where
    parseJSON (Object v) =
        CarPosition <$>
        (v .: "id") <*>
        (v .: "angle") <*>
        (v .: "piecePosition")


-- Helpers
findCar :: String -> [CarPosition] -> Maybe CarPosition
findCar carName = find matches
        where 
            matches carPos = carIdName (posCarId carPos) == carName

