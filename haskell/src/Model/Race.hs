{-# LANGUAGE OverloadedStrings #-}
module Model.Race where

import Data.Aeson ((.:), (.:?), decode, encode, (.=), object, FromJSON(..), ToJSON(..), Value(..))
import Control.Applicative ((<$>), (<*>), (*>), (<|>))

import Model.Car
import Model.Track

data LapTime = LapTime 
        Int -- ^ lap number
        Int -- ^ ticks
        Int -- ^ millis
        deriving Show

data RaceTime = RaceTime 
        Int -- ^ laps
        Int -- ^ ticks
        Int -- ^ millis
        deriving Show

-- RaceSession
data RaceSession = RaceSession 
    { laps :: Maybe Int
    , durationMs :: Maybe Int
    , maxLapTimeMs :: Maybe Int
    , quickRace :: Maybe Bool
    } deriving (Show)

instance FromJSON RaceSession where
    parseJSON (Object v) =
        RaceSession <$>
        (v .:? "laps") <*>
        (v .:? "durationMs") <*>
        (v .:? "maxLapTimeMs") <*>
        (v .:? "quickRace")

-- | Race

data Race = Race 
    { track :: Track
    , cars  :: [Car] 
    , raceSession :: RaceSession
    } deriving (Show)

instance FromJSON Race where
    parseJSON (Object v) =
        Race <$>
        (v .: "track") <*>
        (v .: "cars") <*>
        (v .: "raceSession")
  
