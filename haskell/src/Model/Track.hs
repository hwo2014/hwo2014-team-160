{-# LANGUAGE OverloadedStrings #-}
module Model.Track where

import Control.Applicative ((<$>), (<*>), (*>), (<|>))
import Data.Aeson ((.:), (.:?), decode, encode, (.=), object, FromJSON(..), ToJSON(..), Value(..))
import Data.Maybe (isJust, fromMaybe)

-- | Lane
data Lane = Lane 
    { distanceFromCenter :: Float -- Int
    , index              :: Int
    } deriving (Show)

instance FromJSON Lane where
    parseJSON (Object v) = 
        Lane <$>
        (v .: "distanceFromCenter") <*>
        (v .: "index")


-- | Piece
data Piece = Piece
    { pceLength   :: Maybe Float
    , switch        :: Maybe Bool
    , radius        :: Maybe Float -- Int
    , pieceAngle    :: Maybe Float
    , bridge        :: Maybe Bool 
    } deriving (Show)

instance FromJSON Piece where
  parseJSON (Object v) =
    Piece <$>
    (v .:? "length") <*>
    (v .:? "switch") <*>
    (v .:? "radius") <*>
    (v .:? "angle")  <*>
    (v .:? "bridge")
{-    (Bend <$>
        (v .: "radius") <*>
        (v .: "angle")) <|> 
    (Switch <$>
        (v .: "switch") *>
        (v .: "length")) <|>
    (Straight <$>
        (v .: "length"))
-}

-- Track
data Track = Track 
    { name          :: String
    , startingPoint :: StartingPoint
    , pieces        :: [Piece]
    , lanes         :: [Lane]
    } deriving (Show)

instance FromJSON Track where
    parseJSON (Object v) =
        Track <$>
        (v .: "name") <*>
        (v .: "startingPoint") <*>
        (v .: "pieces") <*>
        (v .: "lanes")

-- StartingPoint
data StartingPoint = StartingPoint 
    { startingPosition  :: Position
    , startingAngle     :: Float
    } deriving (Show)

instance FromJSON StartingPoint where
    parseJSON (Object v) =
        StartingPoint <$>
        (v .: "position") <*>
        (v .: "angle")

-- Position
data Position = Position 
    { x :: Float
    , y :: Float
    } deriving (Show)

instance FromJSON Position where 
    parseJSON (Object v) =
        Position <$>
        (v .: "x") <*>
        (v .: "y")


data Section = 
    Section 
        { sectionPieces         :: [(Int, Piece)]
        , sectionLaneLengths    :: [Float]
        }
    | Switch 
        { switchPiece           :: (Int, Piece)
        , switchLaneLengths     :: [Float]
        }
    deriving Show

isBend pce = isJust (pieceAngle pce) && isJust (radius pce) 

isStraight = not . isBend

isSwitch = isJust . switch

pieceLength :: Piece -> Lane -> Float
pieceLength p@(Piece _ _ _ (Just angle) _) lane = 
    let (Just rad) = trueRadius p lane
    in abs $ 2 * pi * rad * (angle / 360)

pieceLength pce lane = fromMaybe 0.0 (pceLength pce)
    where
        fromCenter = distanceFromCenter lane

trueRadius :: Piece -> Lane -> Maybe Float
trueRadius (Piece _ _ (Just rad) (Just angle) _) lane
    | angle > 0   = Just $ rad - l
    | otherwise = Just $ rad + l
    where l = distanceFromCenter lane
trueRadius _ _ = Nothing


isSwitchSection (Switch _ _) = True
isSwitchSection _           = False

sectionIndex (Switch p _)   = fst p
sectionIndex (Section ps _) = fst $ head ps


-- | Split track into sections separated by Switch
-- Switch is in the end of section.
sections :: Track -> [Section]
-- sections [] = [[]]
sections track = foldr combine []
            $ zipWith (curry section) [0..] (pieces track)
    where 
        pieceLengths lanes pce = map (pieceLength pce) lanes

        combine x [] = [x]
        combine x (a:as) = x `comb` a ++ as

        (Section ap al) `comb` (Section bp bl) = 
                [Section (ap ++ bp) (zipWith (+) al bl)]
        a `comb` b = [a,b]

        section (idx, x) = if isSwitch x 
            then Switch (idx,x) (pieceLengths (lanes track) x) 
            else Section [(idx, x)] (pieceLengths (lanes track) x)


-- | Returns (Just True) if the bend continues to the next Piece
sameBend (Piece _ _ _ (Just a1) _) (Piece _ _ _ (Just a2) _) =
    Just $ signum a1 == signum a2
sameBend _ _ = Nothing

shortestPath track
        = map lengths $ sections track
    where
        lengths = 1
        lanesCount = length $ lanes track

slice :: Int -- ^ from
        -> Int -- ^ slice size
        -> [a] -- ^ List
        -> [a] -- ^ slice
slice _ _ []    = []
slice 0 l (y:ys) 
    | l <= 0    = []
    | l > 0     = y : slice 0 (l-1) ys
slice x l (y:ys) 
    | x < 0     = slice (x+1) (l-1) (y:ys)
    | x > 0     = slice (x-1) l ys

slices :: Int -> Int -> [a] -> [[a]]
slices from len xs = 
    let to = length xs - 1
        ls = [from .. to]
    in map (\i -> slice i len xs) ls
