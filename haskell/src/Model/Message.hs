{-# LANGUAGE OverloadedStrings #-}

-- | Message types between server and client.
--
-- https://helloworldopen.com/techspec
--
module Model.Message where

import Data.Aeson ((.:), (.:?), decode, encode, (.=), object, FromJSON(..), ToJSON(..), toJSON, Value(..))
import Data.Aeson.Types

import Control.Applicative ((<$>), (<*>))
import Control.Monad        (liftM)

import Model.Car
import Model.Race
import Model.Track

-- | Send join message
data Join = Join
    { joinName      :: String      -- ^ Name
    , joinKey       :: String      -- ^ Game Id
    } deriving (Show)

instance ToJSON Join where
    toJSON v = object 
            ["name" .= joinName v
            , "key" .= joinKey v ]

instance FromJSON Join where
    parseJSON (Object v) =
        Join <$>
        (v .: "name") <*>
        (v .: "key")

data Throttle = Throttle 
    { throttle :: Float -- ^ Throttle value, between 0.0 and 1.0.
    } deriving Show

instance ToJSON Throttle where
    toJSON = toJSON . throttle

data Switch = SwitchLeft | SwitchRight deriving Show

instance ToJSON Switch where
    toJSON SwitchLeft = String "Left"
    toJSON SwitchRight = String "Right"


-- | Yourcar

data YourCar = YourCar 
    { yourCarName   :: String 
    , yourCarColor  :: String 
    } deriving (Show)

instance FromJSON YourCar where
    parseJSON (Object v) =
        YourCar <$>
        (v .: "name") <*>
        (v .: "color")

-- GameInit
data GameInit = GameInit
    { race :: Race
    } deriving (Show)

instance FromJSON GameInit where
    parseJSON (Object v) =
        GameInit <$>
        (v .: "race")

data GameStart = GameStart deriving (Show)

instance FromJSON GameStart where
    parseJSON (Object v) = return GameStart

data RaceResult = RaceResult CarId RaceTime deriving (Show)

data GameEnd = GameEnd 
    { results   :: [RaceResult]
    , bestLaps  :: [LapTime]
    } deriving (Show)

data Crash = Crash 
    { crashName     :: String -- ^ Name
    , crashColor    :: String -- ^ Color
    } deriving (Show)

instance FromJSON Crash where
    parseJSON (Object v) =
        Crash <$>
        (v .: "name") <*>
        (v .: "color")

data Spawn = Spawn
    { spawnName     :: String -- ^ Name
    , spawnColor    :: String -- ^ Color
    } deriving (Show)

instance FromJSON Spawn where
    parseJSON (Object v) =
        Spawn <$>
        (v .: "name") <*>
        (v .: "color")
 
data LapFinished = LapFinished
        CarId
        LapTime
        RaceTime
        Int -- ^ Overall
        Int -- ^ fastestLap
        deriving (Show)

data Dnf = Dnf CarId
        String -- ^ Reason
        deriving (Show)

data Finish = Finish CarId deriving (Show)

data BotId = BotId
        String -- ^ Name
        String -- ^ Key
        deriving (Show, Eq)

instance FromJSON BotId where
    parseJSON (Object v) = 
        BotId <$>
        (v .: "name") <*>
        (v .: "key")

instance ToJSON BotId where
    toJSON (BotId name key) = object 
        [ "name" .= name
        , "key"  .= key ]

data CreateRace = CreateRace
        BotId
        (Maybe String)  -- ^ Track name
        (Maybe String)  -- ^ Password
        (Maybe Int)     -- ^ Cars
        deriving (Show, Eq)

data JoinRace = JoinRace
        BotId
        (Maybe String)  -- ^ Track name
        (Maybe String)  -- ^ Password
        Int             -- ^ Cars
        deriving (Show, Eq)

instance FromJSON JoinRace where
    parseJSON (Object v) =
        JoinRace <$>
        (v .: "botId") <*>
        (v .:? "trackName") <*>
        (v .:? "password") <*>
        (v .: "carCount")


instance ToJSON JoinRace where
    toJSON (JoinRace bot track pwd cars) = object
        [ "botId"     .= bot 
        , "trackName" .= track
        , "password"  .= pwd
        , "carCount"  .= cars ]


-- | Ping

data Ping = Ping deriving (Show)

instance ToJSON Ping where
    toJSON v = object [] 

instance FromJSON Ping where
    parseJSON (Object v) = return Ping

-- Helpers
parseSpawn json = decode json :: Maybe Spawn

players :: GameInit -> [CarId]
players gameInit = map carId . cars $ race gameInit

piecesOfGame :: GameInit -> [Piece]
piecesOfGame gameInit = pieces . track $ race gameInit

lanesOfGame :: GameInit -> [Lane]
lanesOfGame gameInit = lanes . track $ race gameInit

reportGameInit :: GameInit -> String
reportGameInit gameInit = "Players: " 
        ++ show ( players gameInit) ++ ", Track: " 
        ++ (show . length $ piecesOfGame gameInit)
        ++ " pieces"


