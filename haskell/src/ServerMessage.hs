{-# LANGUAGE OverloadedStrings #-}
module ServerMessage where

import Data.Functor ((<$>))
import Data.Maybe(fromMaybe, isJust)
import Control.Applicative ((<*>), pure)

import Data.Aeson(decode, FromJSON(..), fromJSON, parseJSON, eitherDecode, Value(..), (.:), (.:?), Result(..))
import Data.Aeson.Types (Parser(..))

import Model.Message
import Model.Car

data ServerMessage = JoinMsg Join
        | JoinRaceMsg JoinRace
        | YourCarMsg YourCar
        | GameInitMsg GameInit
        | GameStartMsg GameStart
        | CarPositionsMsg [CarPosition] Int
        | CrashMsg Crash
--         | GameEndMsg GameEnd
        | TournamentEndMsg
        | UnknownMsg String
        | ErrorMsg String
        deriving Show


instance FromJSON ServerMessage where
    parseJSON (Object v) =
        (,,) <$> (v .: "msgType")
            <*> (v .: "data")
            <*> (v .:? "gameTick")
            >>= parseMessage

parseMessage :: (String, Value, Maybe Int) -> Parser ServerMessage
parseMessage (msgType, msgData, tick)
    | msgType == "carPositions"
            = CarPositionsMsg <$> parseJSON msgData <*> pure (fromMaybe 0 tick)
    | msgType == "gameStart" = pure $ GameStartMsg GameStart
    | msgType == "join" =  JoinMsg <$> parseJSON msgData
    | msgType == "joinRace" =  JoinRaceMsg <$> parseJSON msgData
    | msgType == "yourCar" = YourCarMsg <$> parseJSON msgData
    | msgType == "gameInit" = GameInitMsg <$> parseJSON msgData
    | msgType == "crash" = CrashMsg <$> parseJSON msgData
    | msgType == "error" = ErrorMsg <$> parseJSON msgData
    | msgType == "tournamentEnd" = return TournamentEndMsg
    | otherwise = return . UnknownMsg $ msgType
    --  | otherwise = return . UnknownMsg $ msgType ++ ": " ++ show msgData


