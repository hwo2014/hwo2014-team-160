{-# LANGUAGE DeriveFunctor #-}
module Logic 
    where

import Control.Monad (when)

import Data.Functor ((<$>))
import Data.Maybe(fromMaybe, isJust)


import Util.State

import ClientMessage
import ServerMessage

import Model.Message
import Model.Race
import Model.Track
import Model.Car

data Cycle a = C [a] deriving (Functor, Show)

data Memory = 
    Initial String
    | Memory
        { botName       :: String
        , memThrottle   :: Float
        , memTrack      :: Track
        , memSections   :: Cycle (Int, Section)
        , nextPieces    :: Cycle (Int, Piece)
        , friction      :: Maybe Float
        , memPiece      :: Maybe Piece
        , memPosition   :: Maybe CarPosition
        , lastTick      :: Maybe Int
        , speed         :: Maybe Float
        , switchingTo   :: Maybe Int
        }
    | EndMem
        { friction      :: Maybe Float
        }
    deriving Show

type LogicHandler = ServerMessage -> Memory -> IO ([ClientMessage], Memory)
-- ServerMessage -> StateT Memory IO [ClientMessage]

botName' m f = let val = f (botName m) in (m{botName = val}, val)
memThrottle' m f = let val = f (memThrottle m) in (m{memThrottle = val}, val)
memTrack' m f = let val = f (memTrack m) in (m{memTrack = val}, val)
memPosition' m f = let val = f (memPosition m) in (m{memPosition = val}, val)
lastTick' m f = let val = f (lastTick m) in (m{lastTick = val}, val)

(^!) :: Monad m => (x -> f -> (x, b)) -> f -> StateT x m ()
prop ^! f = modify (fst . flip prop f)

throttleMessage = ThrottleMsg . Throttle
switchLaneMessage = SwitchMsg
pingMessage = PingMsg Ping

initial = Initial
{-
handle :: LogicHandler 
        -> ServerMessage -> Memory 
        -> IO ([ClientMessage], Memory)
handle h mes = runState $ h mes
-}

logicHandler :: LogicHandler 
logicHandler (JoinMsg join) mem = do
        putStrLn "Joined"
        return ([pingMessage], mem)

logicHandler (JoinRaceMsg join) mem = do
        putStrLn "Joined race"
        return ([pingMessage], mem)
        
logicHandler (YourCarMsg car) mem = do
        putStrLn $ "My color is " ++ yourCarColor car
        return ([pingMessage], mem)

logicHandler (GameInitMsg gameInit) mem = do
        putStrLn $ "GameInit: " ++ reportGameInit gameInit
        let mem' = memoryFromGameInit gameInit mem
        return ([pingMessage], mem')

logicHandler (UnknownMsg "lapFinished") mem = do
        putStrLn "Lap finished"
        return ([pingMessage], mem)
        -- return ([switchLaneMessage SwitchRight], mem)
logicHandler (GameStartMsg GameStart) mem = 
        return ([throttleMessage 1.0], mem{memThrottle = 1.0})
logicHandler (CarPositionsMsg ps tick) mem = 
        handleCarPositionChange ps tick mem

logicHandler (CrashMsg c) mem = do
        putStrLn $ "Speed: " ++ show (speed mem)
        putStrLn $ "Throttle: " ++ show (memThrottle mem)
        putStrLn $ "Radius: " ++ show (radius <$> memPiece mem)
        putStrLn $ "Angle: " ++ show (carAngle <$> memPosition mem)
        
        return $ if crashName c == botName mem 
            then ([pingMessage], mem{lastTick = Nothing})
            else ([pingMessage], mem)

logicHandler (ErrorMsg str) mem = do
        putStrLn $ "Throttle: " ++ show (memThrottle mem)
        return ([pingMessage], mem)

logicHandler (TournamentEndMsg) mem = 
        return ([Close], mem)

logicHandler s mem = do
        print s
        return ([pingMessage], mem)

memoryFromGameInit :: GameInit -> Memory -> Memory
memoryFromGameInit game (Initial name) = 
        Memory name 0.63 
            track' 
            (C . zip [0..] $ sections track')
            (C . zip [0..] $ pieces track')
            Nothing Nothing
            Nothing Nothing Nothing Nothing
        where
            track' = track $ race game

memoryFromGameInit _ x = x

-- | Handle stuff when car position changes
handleCarPositionChange _ _ (Initial s) = do
        putStrLn "Initial and gameposition."
        return ([pingMessage], Initial s)

handleCarPositionChange ps tick mem =
--            when (tick `mod` 10 == 0) $!
--                putStrLn $ "-Speed: " ++ show speed'
--            when changeFriction $!
--                putStrLn $ show friction' ++ " in " 
--                                ++  show speed'
--                                ++ " -> " ++ show lastSpeed
            return ([throttleMessage throttle'], mem')
        where
            (Just pos) = findCar (botName mem) ps

            ticks = max 1 $ tick - fromMaybe tick (lastTick mem)

            track' = memTrack mem
            pieces' = pieces track'

            lastThrottle = memThrottle mem
            lastSpeed   = speed mem
            
            lastPos     = memPosition mem
            lastPieceP  = piecePosition <$> lastPos
            lastIdx     = fromMaybe 0 $! pieceIndex <$> lastPieceP
            lastDistance = fromMaybe 0 $! inPieceDistance <$> lastPieceP

            C ((lastSectionIdx, lastSection):_) = memSections mem

            newPieceP   = piecePosition pos
            newIdx      = pieceIndex newPieceP
            newDistance = inPieceDistance newPieceP

            äää = nextSectionIndex $ memSections mem

            -- Update nextPieces
            (C nextPieces') = if lastIdx == newIdx
                    then nextPieces mem
                    else cycle1 $ cycleUntil ((==) newIdx . fst) $ nextPieces mem

            (Just newPiece) = if lastIdx == newIdx && isJust (memPiece mem)
                    then memPiece mem
                    else Just $ pieces' !! newIdx

            newLaneStart = startLaneIndex $! lane newPieceP
            lane' = lanes track' !! newLaneStart
            fromCenter' = distanceFromCenter lane'

            maybeNextLane = (lanes track' !!) <$> switchingTo mem

            lastToEnd = pieceLength (pieces' !! lastIdx) lane' 
                            - lastDistance

            -- Update speed
            speed' :: Float
            speed' = if lastIdx == newIdx
                    then (newDistance - lastDistance) / fromIntegral ticks
                    else (lastToEnd + newDistance) / fromIntegral ticks

            -- Update friction
            changeFriction = lastThrottle <= 0 && isJust lastSpeed



--            friction' = fromMaybe 0.2 $! 
--                    if changeFriction
--                    then
--                        let (Just old) = friction mem
--                            (Just s)   = lastSpeed 
--                        in Just $ (old * 2.0 + s - (speed' * 0.75)) / 3.0
--                    else friction mem

            friction' = 0.24
        
            angle = carAngle pos

            toPieceEnd = pieceLength newPiece lane' - newDistance

            untilNextBend = toPieceEnd + toNextBend lane' nextPieces'

            nextBend' = nextBend nextPieces'

            -- Bend is over or changes direction
            bendIsOver = toPieceEnd < inPieceEnd && 
                    (untilNextBend > toPieceEnd + 1
                     || bendChange)
                where
                    (Just radius') = radius newPiece
                    (Just angle') = pieceAngle newPiece
                    inPieceEnd = 80.0 * (radius' / 100) * (angle' / 45)
                    bendChange = maybe False not $ sameBend newPiece nextBend'



            factor                  = 0.97
            driftingFactor          = 1.0
            minBendingThrottle      = 0.7
            bendingThrottleFactor   = 1.0
            bendingSpeedFactor      = 6.6
            maxAngle                = 57

            maxBendingThrottle = minBendingThrottle 
                            + (max' - minBendingThrottle) 
                            * (((maxAngle - angle') / maxAngle) * driftingFactor)
                        where
                            angle' = abs angle
                            (Just rad) = trueRadius newPiece lane'
                            max' = min 1.0 $! bendingThrottleFactor * rad / 100 * factor

            maxBendingSpeed = case radius' of
                    90  -> 6.48  -- .52
                    110 -> 7.204 -- .208
                    190 -> 10
                    210 -> 10
                    _   -> 5
                where (Just radius') = trueRadius newPiece lane'
 
--            maxBendingSpeed = let (Just rad) = trueRadius newPiece lane'
--                        in bendingSpeedFactor / rad * 100 * factor

            maxNextBendingSpeed = case radius' of
                    90  -> 6.68
                    110 -> 7.53
                    190 -> 10
                    210 -> 10
                    _   -> 5
                where (Just radius') = trueRadius nextBend' lane'

--            maxNextBendingSpeed = bendingSpeedFactor * pieceRadius / 100 * factor
--                where (Just pieceRadius) = trueRadius nextBend' lane'

            -- https://answers.yahoo.com/question/index?qid=20110827201537AAKjUGv
            -- v² = u² + 2a * d
            u = speed'
            v = maxNextBendingSpeed
            a = friction'
            dis = ((v - u) * (v + u)) / (-2 * a)

            -- ticksToBend = (untilNextBend - speed') / speed'
            -- needToBrake = speed' - maxNextBendingSpeed
            -- timeToBrake = needToBrake / friction' <= ticksToBend

            timeToBrake = untilNextBend <= dis

            bendingThrottle
                | speed' <= 0.01 = maxBendingThrottle
                | speed' >= maxBendingSpeed = 0.0
                | abs angle > maxAngle = 0.50
                | bendIsOver && not timeToBrake = 1.0
                -- | beginningOfBend = 0.0
                | otherwise  = maxBendingThrottle


            straightThrottle
                | speed' <= 0.01 = 1.0
                | speed' < maxNextBendingSpeed = 1.0
                | timeToBrake = 0.0
                -- | untilNextBend < 100.0 = 0.0
                | otherwise = 1.0

            throttle' = if isBend newPiece
                    then bendingThrottle
                    else straightThrottle 

            -- Only switch when throttle is unchanged
            canSwitch = lastThrottle == throttle'

            -- | Update position and speed
            mem' = mem
                { memPosition = Just pos 
                , speed = Just speed'
                , memPiece = Just newPiece
                , nextPieces = C nextPieces'
                , lastTick = Just tick
                , memThrottle = throttle'
                , friction = Just friction'
                }

nextBend :: [(a, Piece)] -> Piece
nextBend pieces = snd . head $ dropWhile (not . isBend . snd) pieces

toNextBend :: Lane -> [(a, Piece)] -> Float
toNextBend lane pieces = sum .
                map (flip pieceLength lane . snd) 
                    $ takeWhile (not . isBend . snd) pieces

-- | Index of piece in beginning of next section.
nextSectionIndex :: Cycle (a, Section) -> Int
nextSectionIndex ss = 
        let (C (x:xs)) = cycle1 ss
        in sectionIndex $ snd x

-- | Index of next switch in beginning of next section.
nextSwitchIndex :: Cycle (a, Section) -> Int
nextSwitchIndex ss = 
        let (C (x:xs)) = cycleUntil isSwitchSection $ fmap snd ss
        in sectionIndex x

cycle1 :: Cycle a -> Cycle a
cycle1 (C []) = C []
cycle1 (C (x:xs)) = C $ xs ++ [x]


cycleUntil :: (a -> Bool) -> Cycle a -> Cycle a
cycleUntil _ (C []) = C []
cycleUntil p (C (x:xs))
        | p x       = C $ x:xs
        | otherwise = cycleUntil p . C $ xs ++ [x]
