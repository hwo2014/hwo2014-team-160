-- | Rewritten state monad
module Util.State where
import Control.Monad (ap)
import Control.Applicative

newtype StateT s m a = StateT { runState :: s -> m (a,s) }

instance Functor m => Functor (StateT s m) where
    fn `fmap` state = StateT $ \s -> (\(a,s) -> (fn a,s)) `fmap` runState state s


instance (Applicative m, Monad m) => Applicative (StateT s m) where
    (<*>) = ap
    pure = return 

instance (Monad m) => Monad (StateT s m) where
    x >>= f = StateT $ \s -> do
        (a, s') <- runState x s
        runState (f a) s'

    return x = StateT $ \s -> return (x,s)

    fail str = StateT $ \_ -> fail str

lift m = StateT $ \s -> do
       x <- m
       return (x, s)

get :: (Monad m) => StateT s m s
get = StateT $ \s -> return (s,s)

put :: (Monad m) => s -> StateT s m ()
put s = StateT $ \_ -> return ((), s)

modify :: (Monad m) => (s -> s) -> StateT s m ()
modify f = StateT $ \s -> return ((), f s)
