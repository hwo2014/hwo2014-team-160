module Bot where

-- | Speed is length per a tick.
speed length ticks = length / ticks


-- | Other bots and their position and speed
otherBots = []

-- | Handle when someone crashed
